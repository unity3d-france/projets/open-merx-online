﻿using System;
using System.Collections.Generic;

using UnityEngine;

[Serializable]
public class ResourceInfo : PropertyAttribute {
    public int ID;
    public string name;
    public Sprite icon;    
}

[Serializable]
public class StationLocalInfo {
    public int ID;
    public string name;
    public Sprite icon;
    public string description;
}

[Serializable]
public class VehicleInfo {
    public int type;
    public string name;
    public Sprite icon;
}

public class ResourcesData : MonoBehaviour {

    public static ResourcesData Instance { get; private set; }



    [SerializeField]
    List<ResourceInfo> resources = new List<ResourceInfo>();

    [SerializeField]
    List<StationLocalInfo> stations = new List<StationLocalInfo>();

    [SerializeField]
    List<VehicleInfo> vehicles = new List<VehicleInfo>();

    private void Awake() {
        if (Instance != null)
            Destroy(this);

        Instance = this;
    }

    public Sprite GetSprite(int ID) {
        return resources[ID-1].icon;
    }

    public StationLocalInfo GetStationInfos(int id) {
        foreach(StationLocalInfo s in stations) {
            if(s.ID == id) {
                return s;
            }
        }

        return null;
    }

    public Dictionary<int,ResourceInfo> GetResources() {
        Dictionary<int, ResourceInfo> result = new Dictionary<int, ResourceInfo>();

        foreach(ResourceInfo r in resources) {
            if (!result.ContainsKey(r.ID)) {
                result.Add(r.ID, r);
            }
        }

        return result;
    }

    public VehicleInfo GetVehicleInfo(int type) {
        foreach(VehicleInfo v in vehicles) {
            if(v.type == type) {
                return v;
            }
        }

        return null;
    }
}
