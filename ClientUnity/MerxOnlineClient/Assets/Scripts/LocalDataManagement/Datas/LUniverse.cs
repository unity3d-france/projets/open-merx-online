﻿using System.Collections.Generic;

using MerxStructures;
using MerxMessage;

public class LUniverse {

    #region events
    public delegate void intAction(long frame);
    public delegate void StationAction(LCity station);
    public delegate void HangarAction(LHangar station);

    public event intAction OnTimeChange = delegate { };
    public event StationAction OnNewStation = delegate { };
    public event HangarAction OnNewHangar = delegate { };

    #endregion

    private long _frame = 0;
    public long Frame { get { return _frame; } set { _frame = value; OnTimeChange(_frame); } }

    private Dictionary<int, LCity> _cities = new Dictionary<int, LCity>();
    private Dictionary<int, HashSet<int>> _gates = new Dictionary<int, HashSet<int>>();
    private Dictionary<int, LCorporation> _corporations = new Dictionary<int, LCorporation>();

    LocalDataManager _manager = null;

    public LUniverse(LocalDataManager manager) {
        _manager = manager;
    }

    public LCity CreateCity(CityInfo infos) {
        if (!_cities.ContainsKey(infos.ID)) {
            LCity s = new LCity(infos.ID,_manager);
            _cities.Add(infos.ID, s);
            s.OnNewHangar += (h) => OnNewHangar(h);
            OnNewStation(s);            
        }

        return _cities[infos.ID];
    }

    public LCorporation CreateCorporation(CorporationInfos infos) {
        if (!_corporations.ContainsKey(infos.CorpID)) {
            _corporations.Add(infos.CorpID, new LCorporation(_manager));
        }
        return _corporations[infos.CorpID];
    }

    public LCorporation GetCorporatiopn(int id) {
        if (_corporations.ContainsKey(id))
            return _corporations[id];
        return null;
    }

    public LCity GetCity(int id) {
        if (_cities.ContainsKey(id))
            return _cities[id];

        return null;
    }

    public void AddGate(int fromStation, int toStation) {

    }
}
