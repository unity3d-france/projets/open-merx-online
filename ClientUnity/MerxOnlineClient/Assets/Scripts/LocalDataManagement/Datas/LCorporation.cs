﻿public class LCorporation {

    public delegate void OnChangeAction();
    public event OnChangeAction OnChange = delegate { };

    LocalDataManager _manager = null;

    public long ICU { get; set; }

    public string Name { get; private set; }

    public int HomeCityID { get; private set; }

    public LCorporation(LocalDataManager manager) {
        _manager = manager;
    }

    public void Update(MerxStructures.CorporationInfos infos) {
        ICU = infos.Icu;
        Name = infos.Name;
        HomeCityID = infos.HomeCity;

        OnChange();
    }
}
