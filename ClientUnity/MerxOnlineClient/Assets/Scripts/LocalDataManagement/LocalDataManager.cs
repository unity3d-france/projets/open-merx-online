﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using MerxMessage;
using MerxStructures;

/// <summary>
/// c'est le component qui permet d'avoir un point d'entrée unique pour récuperer les objects
/// qui contiennent les données corespondant aux informations envoyées par le serveur.
/// </summary>
public class LocalDataManager : MonoBehaviour {

    /// <summary> permet de reduire les finds </summary>
    static LocalDataManager _instance = null;
    public static LocalDataManager Instance { get {
            return _instance;
        }
    }

    //l'univers du jeu
    public LUniverse Universe { get; private set; }

    /// <summary> Permet de savoir si les données du serveur on été recues </summary>
    public bool DataOK { get; private set; }

    /// <summary> permet de savoir si la sequence de recuperation de l'organisation est terminee ou pas </summary>
    public bool OrganisationOK { get; private set; }

    public delegate void OnNewMessageAction(Message m);
    public event OnNewMessageAction OnNewMessage = delegate { };

    // map des methodes a appeler en fonction des type de messages recu
    Dictionary<Type, OnNewMessageAction> messageProcessCB = new Dictionary<Type, OnNewMessageAction>();

    //communication
    private IRequestProxy _requestProxy;
    private IEventProxy _eventProxy;

    private float _deltaTime = 0.0f;
    private int _localPlayerID = 0;
    public int LocalPlayerID { get { return _localPlayerID; }  set { _localPlayerID = value; } }
    
    public LCorporation LocalCorporation { get; private set; }
    public LMailBox LocalMailbox { get; private set; }

    private LPlayerHangarsList _playerHangarList = null;
    public LPlayerHangarsList PlayerHangarList{
        get {
            return _playerHangarList;
        }
    }

    public LCitiesList CitiesList { get; private set; }

    private void Awake() {
        if (null != _instance) {
            Destroy(gameObject);
            return;
        }
        _instance = this;

        //preparation des callback pour les message
        messageProcessCB.Add(typeof(RequestBuyHangar), ProcessBuyHangar);
        messageProcessCB.Add(typeof(RequestCorporationUpdate), ProcessCorporationUpdate);
        messageProcessCB.Add(typeof(RequestFillOrders), null);
        messageProcessCB.Add(typeof(RequestHangarList), null);
        messageProcessCB.Add(typeof(RequestHangarUpdate), ProcessHangarUpdate);
        messageProcessCB.Add(typeof(RequestMarketDatas), null);
        messageProcessCB.Add(typeof(RequestResourceMove), null);
        messageProcessCB.Add(typeof(RequestCities), ProcessCities);
        messageProcessCB.Add(typeof(RequestUniverseTime), ProcessUniverseTime);
        messageProcessCB.Add(typeof(RequestMailBoxUpdate), ProcessMailBoxUpdate);

        // lancement de la creatoin des donnes
        Universe = new LUniverse(this);  
    }
    
    private void Start() {
        InvokeRepeating("RequestUniverseTime", 0.0f, 10.0f);
    }

    public void SetProxy(IEventProxy eventProxy, IRequestProxy requestProxy) {
        _eventProxy = eventProxy;
        _requestProxy = requestProxy;               
    }


    public void GetDatas() {
        StartCoroutine(GetDatasSequence());
    }

    private IEnumerator GetDatasSequence() {

        int loop = 0;

        //récuperer les cites
        CitiesList = new LCitiesList(this);
        while (CitiesList.GetCities().Count == 0) {
            loop++;
            yield return null;

            if (loop > 1000) {
                loop = 0;
                _requestProxy.SendRequest(new RequestCities());
            }
        }
        Debug.Log("********* city count : " + CitiesList.GetCities().Count);
        DataOK = true;

        yield break;
    }

    public void GetOrganisationDatas() {
        OrganisationOK = false;
        StartCoroutine(GetOrganisationSequence());
    }

    private IEnumerator GetOrganisationSequence() {

        int loop = 0;

        //demande des donnes de depart
        RequestCorporationUpdate corpUpdate = new RequestCorporationUpdate();
        bool resultReceived = false;
        OnNewMessageAction a = (m) => {
            if (m is RequestCorporationUpdate) resultReceived  = true;
        };
        OnNewMessage += a;

        corpUpdate.corpID = LocalPlayerID;
        _requestProxy.SendRequest(corpUpdate);
        loop = 0;
        
        while(LocalCorporation == null && !resultReceived) {
            yield return null;
            loop++;
            if (loop >= 1000) {
                loop = 0;
                corpUpdate = new RequestCorporationUpdate();
                corpUpdate.corpID = LocalPlayerID;
                _requestProxy.SendRequest(corpUpdate);
            }
        }
        OnNewMessage -= a;
        if (LocalCorporation == null) {
            // on a recu le message et pas de corporation, ca veut dire que l'organisation n'existe pas encore
            OrganisationOK = true;
            yield break;
        }        

        Debug.Log("************* received Local Corporation ***********");

        // recuperer la liste des hangars du joueurs
        _playerHangarList = new LPlayerHangarsList(_localPlayerID, this);
        RequestHangarList request = new RequestHangarList();
        _requestProxy.SendRequest(request);
        while (_playerHangarList.IsLoading) {
            yield return null;
            loop++;
            if (loop >= 1000) {
                loop = 0;
                request = new RequestHangarList();
                _requestProxy.SendRequest(request);
            }
        }
        Debug.Log("************ Hangar list OK ************ ");

        RequestMailBoxUpdate r = new RequestMailBoxUpdate();
        SendRequest(r);
        loop = 0;
        while (LocalMailbox == null) {
            yield return null;
            loop++;
            if(loop >= 1000) {
                loop = 0;
                r = new RequestMailBoxUpdate();
                SendRequest(r);
            }
        }

        Debug.Log("**************** MailBox : OK **********");
        OrganisationOK = true;
        yield break;
    }
    
/// <summary>
/// permet de s'assurer que le moteur de donnee locale a les informations necessaires pour lancer le jeu
/// </summary>
/// <returns></returns>
    public bool Ready() {

        if (LocalPlayerID == -1)
            return false;

        LCorporation corp = Universe.GetCorporatiopn(LocalPlayerID);
        if (null == corp)
            return false;

        if (null == LocalMailbox) {
            return false;
        }

        return true;
    }

    private void Update() {
        if (_eventProxy == null || null == _requestProxy)
            return;

        _deltaTime += Time.deltaTime;
        while (_deltaTime > 0.01f) {
            Universe.Frame++;
            _deltaTime -= 0.01f;
        }

        //manage messages
        Queue<Message> message = _eventProxy.GetMessages();
        while(message.Count > 0) {
            Message m = message.Dequeue();

            // traiter les donnes
            if (messageProcessCB.ContainsKey(m.GetType())) {
                messageProcessCB[m.GetType()]?.Invoke(m);
            }
            
            //informer les gens qui suivent les messages
            OnNewMessage(m);
        }
    }

    public void SendRequest(Message m) {
        _requestProxy.SendRequest(m);
    }

    public void RequestCitiesList() {
        RequestCities request = new RequestCities();

        foreach(CityInfo i in request.cities) {
            //todo update la station
        }
        for(int i = 0; i < request.gatesFrom.Count; i++) {
            Universe.AddGate(request.gatesFrom[i], request.gatesTo[i]);
            Universe.AddGate(request.gatesTo[i], request.gatesFrom[i]);
        }
        _requestProxy.SendRequest(request);
    }

    private void RequestUniverseTime() {
        if (null != _requestProxy) {
            RequestUniverseTime req = new RequestUniverseTime();
            _requestProxy.SendRequest(req);
        }
    }

    private void ProcessBuyHangar(Message m) {
        RequestBuyHangar r = m as RequestBuyHangar;
        if (null == r)
            return;

        LCity targetStation = Universe.GetCity(r.CityID);
        if (null == targetStation)
            return;

        LHangar h =  targetStation.AddHangar(r.FromPlayerID);
        h.Update(r.Hangar);

    }

    private void ProcessCorporationUpdate(Message m) {
        RequestCorporationUpdate r = m as RequestCorporationUpdate;
        if (null == r)
            return;

        if (null == r.corpInfo)
            return; //la corporation demandée n'existe pas

        LCorporation corp = Universe.GetCorporatiopn(r.corpInfo.CorpID);
        if(null == corp) {
            corp = Universe.CreateCorporation(r.corpInfo);
        }
        corp.Update(r.corpInfo);

        if(r.corpInfo.CorpID == LocalPlayerID)
            LocalCorporation = corp; 
    }

    private void ProcessHangarUpdate(Message m) {
        RequestHangarUpdate r = m as RequestHangarUpdate;
        if (null == r)
            return;

        LCity s = Universe.GetCity(r.infos.cityID);
        if (null == s)
            return;

        LHangar h = s.GetHangar(r.infos.corpID);
        if (null == h)
            return;

        h.Update(r.infos);
    }

    private void ProcessCities(Message m) {
        RequestCities r = m as RequestCities;
        if (null == r)
            return;

        foreach (CityInfo i in r.cities) {
            LCity s = Universe.GetCity(i.ID);
            if(null == s) {
                s = Universe.CreateCity(i);
            }
            s.Update(i);
        }
    }

    private void ProcessUniverseTime(Message m) {
        RequestUniverseTime r = m as RequestUniverseTime;
        if (null == r)
            return;

        Universe.Frame = r.Frame;
    }

    private void ProcessMailBoxUpdate(Message m) {
        RequestMailBoxUpdate r = m as RequestMailBoxUpdate;
        if (null == r)
            return;

        if (LocalMailbox == null) {
            LocalMailbox = new LMailBox();
        }
        LocalMailbox.UpdateInfos(r.infos);
    }
}
