﻿using System;
using System.Collections.Generic;

using UnityEngine;

public class HangarListWidget : WindowContentPrefab {

    [SerializeField]
    HangarLineInfoWidget _linePrefa = null;

    [SerializeField]
    Transform _content = null;

    LocalDataManager _localDataManager = null;
    LPlayerHangarsList _hangarList = null;

    private void Awake() {
        _localDataManager = LocalDataManager.Instance;
        _hangarList = _localDataManager.PlayerHangarList;
        if (null != _hangarList) {
            _hangarList.OnLoadingDone += OnHangarListLoad;
            _hangarList.OnChange += OnHangarListLoad;
            OnHangarListLoad();
        }
    }

    private void Start() {
        _window.SetLoading(_hangarList.IsLoading);
        _window.Title = "My Hangars";
    }

    private void OnDestroy() {
        if(null != _hangarList) {
            _hangarList.OnLoadingDone -= OnHangarListLoad;
            _hangarList.OnChange -= OnHangarListLoad;
        }
    }

    private void OnHangarListLoad() {
        while(_content.childCount > 0) {
            Transform t = _content.GetChild(0);
            t.SetParent(null);
            Destroy(t.gameObject);
        }
        foreach(LHangar h in _hangarList.GetHangars()) {
            HangarLineInfoWidget line = Instantiate<HangarLineInfoWidget>(_linePrefa);
            line.SetHangar(h);
            line.transform.SetParent(_content);
        }
    }
}
