﻿using UnityEngine;
using UnityEngine.UI;

public class HangarLineInfoWidget : MonoBehaviour {

    LHangar _hangar = null;

    [SerializeField]
    TMPro.TextMeshProUGUI _CityName = null;

    private void Awake() {
        Button b = GetComponent<Button>();
        b.onClick.AddListener(OnClic);
    }

    public void SetHangar(LHangar hangar) {
        _hangar = hangar;
        _CityName.text = hangar.City.Name;
    }

    private void UpdateVisu() {
        if (null == _hangar)
            return;
    }

    private void OnClic() {
        WindowSystem ws = FindObjectOfType<WindowSystem>();
        Window w = ws.NewWindow("HangarView");
        HangarWidget widget = w.Content.GetComponentInChildren<HangarWidget>();
        widget.SetHangar(_hangar);
        w.Show();
    }
}
