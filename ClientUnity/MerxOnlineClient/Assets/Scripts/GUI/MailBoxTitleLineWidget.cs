﻿using UnityEngine;
using UnityEngine.UI;


public class MailBoxTitleLineWidget : MonoBehaviour
{
    [SerializeField]    
    Button _deleteButton = null;

    [SerializeField]
    TMPro.TextMeshProUGUI _text = null;

    MerxStructures.MailInfos _mail = null;

    private void Start() {
        if(null != _deleteButton) {
            _deleteButton.onClick.AddListener(OnClic);
        }
    }

    public void SetMessage(MerxStructures.MailInfos mail) {
        _mail = mail;
        _text.text = mail.Subject;
        if (mail.Read) {
            _text.color = Color.white;
        } else {
            _text.color = Color.green;
        }
    }

    private void OnClic() {
        MerxMessage.RequestMailBoxDeleteMessage message = new MerxMessage.RequestMailBoxDeleteMessage();
        message.Mail = _mail;

        LocalDataManager.Instance.SendRequest(message);
    }
}
