﻿using UnityEngine;

public class HeaderWidget : MonoBehaviour {

    [SerializeField]
    TMPro.TextMeshProUGUI _date = null;

    [SerializeField]
    TMPro.TextMeshProUGUI _ICU = null;

    [SerializeField]
    TMPro.TextMeshProUGUI _Name = null;

    LCorporation _localCorp = null;

    public void SetDate(long date) {
        if (null == _date)
            return;

        _date.text = date.ToString();
    }

    public void SetLocalCorporation(LCorporation corp) {
        _localCorp = corp;
        if (null != _localCorp) {
            _localCorp.OnChange += Corp_OnChange;
        }
        Corp_OnChange();
    }

    private void Corp_OnChange() {
        if (null == _localCorp)
            return;

        if(null != _ICU)
            _ICU.text = _localCorp.ICU.ToString() + " ICU";

        if (null != _Name)
            _Name.text = _localCorp.Name;
    }
}
