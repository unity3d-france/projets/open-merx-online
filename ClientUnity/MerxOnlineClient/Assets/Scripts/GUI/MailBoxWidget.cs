﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MailBoxWidget : WindowContentPrefab {

    LMailBox _box = null;

    [SerializeField]
    GameObject titleLinePrefab = null;

    [SerializeField]
    Transform titleContainer = null;

    [SerializeField]
    TMPro.TextMeshProUGUI mailContent = null;

    /// <summary> l'id du mail actuellement selectionné </summary>
    int _currentSelection = -1;

    private void Start() {
        _box = LocalDataManager.Instance.LocalMailbox;

        _box.OnChange += UpdateVisu;

        UpdateVisu();
    }

    private void OnDestroy() {
        if(null != _box) {
            _box.OnChange -= UpdateVisu;
        }
    }

    private void UpdateVisu() {
        // nettoyer
        Clear();

        //mettre a jour
        if (null == _box)
            return;

        bool currentStillExist = false;

        foreach(MerxStructures.MailInfos m in _box.GetMails()) {
            GameObject g = Instantiate(titleLinePrefab);
            g.transform.SetParent(titleContainer);

            //ajouter un "on clic" sur le texte
            Button b = g.GetComponent<Button>();
            b.onClick.AddListener(() => { OnLineClic(m); });

            MailBoxTitleLineWidget line = g.GetComponent<MailBoxTitleLineWidget>();
            line.SetMessage(m);

            if(m.Id == _currentSelection) {
                currentStillExist = true;
            }
        }

        if (!currentStillExist) {
            _currentSelection = -1;
            mailContent.text = "";
        }
    }

    private void Clear() {
        while (titleContainer.childCount > 0) {
            Transform t = titleContainer.GetChild(0);
            t.SetParent(null);
            Destroy(t.gameObject);
        }
    }

    private void OnLineClic(MerxStructures.MailInfos mail) {
        mailContent.text = mail.Content;
        ContentSizeFitter c = mailContent.GetComponent<ContentSizeFitter>();
        c.SetLayoutVertical();

        _currentSelection = mail.Id;

        if (!mail.Read) {
            MerxMessage.RequestMailBoxReadMail m = new MerxMessage.RequestMailBoxReadMail();
            m.Id = mail.Id;
            LocalDataManager.Instance.SendRequest(m);
        }
    }
}
