﻿using UnityEngine;
using UnityEngine.UI;

using MerxStructures;

public class MarketBuyerLineWidget : MonoBehaviour {

    [SerializeField]
    TMPro.TextMeshProUGUI qte = null;

    [SerializeField]
    TMPro.TextMeshProUGUI price = null;

    [SerializeField]
    TMPro.TextMeshProUGUI city = null;

    [SerializeField]
    Button sellButton = null;

    MarketOrder _order = null;

    private void Awake() {
        sellButton.onClick.AddListener(OnSell);
    }

    public void SetOrder(MarketOrder order) {
        qte.text = order.qte.ToString();
        price.text = order.price.ToString() + "ICU";
        LCity s = LocalDataManager.Instance.Universe.GetCity(order.cityID);
        city.text = s.Name;

        sellButton.interactable = true;

        _order = order;
    }

    void OnSell() {
        if (_order == null)
            return;

        MarketWidget marketWidget = GetComponentInParent<MarketWidget>();
        marketWidget.SelectOrder(_order);
    }
}
