﻿using System;
using System.Threading;
using MerxMessage;
using MerxServer.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ServerApp.Config;

namespace ServerApp {
    public class App {
        private readonly SFSConnection _sFSConnection;
        private readonly ILogger _logger;
        private readonly IMerxServerApp _merxServerApp;
        private readonly ServerSettings _configuration;
        private string filename;
        public bool ApplicationRunning { get; set; }
        

        public App(SFSConnection sFSConnection, ILogger<App> logger, IMerxServerApp merxServerApp, IOptions<ServerSettings> configuration) {
            _sFSConnection = sFSConnection;
            _logger = logger;
            _merxServerApp = merxServerApp;
            _configuration = configuration.Value;
            filename = _configuration.SaveFile;
            //event to send the message receivend fomr the connection
            _sFSConnection.OnNewMessage += _conn_OnNewMessage;
        }
        
        public void Run() {

            string str = "*** OpenMerxOnline Server v" + _configuration.Version + " ***";
            System.Console.WriteLine(str);
            _logger.LogInformation(str);
            str = "Environnement " + Environment.GetEnvironmentVariable("ENVIRONMENT");
            System.Console.WriteLine(str);
            _logger.LogInformation(str);

            //Thread qui écoute ce qui est saisi dans la console
            if (!_configuration.DaemonMode)
                new Thread(Command).Start();

            //LA connection est lancée + login + roomjoin. On boucle tant que tout ceci n'est pas réalisé
            _sFSConnection.Connect();
            while (!_sFSConnection.Ready()) {
                _sFSConnection.Process();
                System.Threading.Thread.Sleep(0);
            }

            //Tuning MerxServerApp threadLoop
            _merxServerApp.InitMerxServerTuningLoop(_configuration.msPerFrame, _configuration.ThreadLoopSleep);

            //On peut initialiser l'univers
            _logger.LogInformation("Universe Loading");
            if (System.IO.File.Exists(filename)) {
                _merxServerApp.InitUniverseManager(System.IO.File.ReadAllBytes(filename));
            } else {
                _merxServerApp.InitUniverseManager();
            }

            //Démmarage du serveur
            _merxServerApp.Start();
            ApplicationRunning = true;
            Console.WriteLine("Game Server Started - type help for a list of commands");

            //boucle qui écoute ce qui arrive de smartfox
            while (ApplicationRunning) {
                _sFSConnection.Process();
                System.Threading.Thread.Sleep(10);
                if (System.IO.File.Exists("shutdown.txt"))
                    break;
            }

            _logger.LogInformation("Shutting down and saving");

            _merxServerApp.Stop();

            WriteSaveFile();

            System.IO.File.Delete("shutdown.txt");

        }

        private void Command() {
            bool runningthread = true;
            while (runningthread) {

                string command = Console.ReadLine();
                switch (command) {
                    case "players":
                        _sFSConnection.CommandUserList();
                        break;
                    case "stop":
                        ApplicationRunning = false;
                        runningthread = false;
                        break;
                    case "help":
                        DisplayHelp();
                        break;
                    case "save":
                        WriteSaveFile();
                        break;
                    default:
                        break;
                }
            }
        }

        private void WriteSaveFile() {
            if (System.IO.File.Exists(filename)) {
                string date = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss");
                Console.WriteLine(date + "_" + filename);
                System.IO.File.Move(filename, date + "_" + filename);
            }
            var saveData = _merxServerApp.GetSaveData();
            if (saveData != null) {
                System.IO.File.WriteAllBytes(filename, _merxServerApp.GetSaveData());
                Console.WriteLine("Universe saved");
                _logger.LogInformation("Universe saved");
            } else {
                Console.WriteLine("Universe not saved");
                _logger.LogError("Universe not saved");
            }
        }

        private void DisplayHelp() {
            Console.WriteLine("SERVER CONSOLE COMMANDS:");
            Console.WriteLine(" players");
            Console.WriteLine(" save");
            Console.WriteLine(" stop");
        }

        private void _conn_OnNewMessage(Message m) {
            _merxServerApp.ProcessMessage(m);
        }
    }
}
