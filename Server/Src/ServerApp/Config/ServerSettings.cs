﻿
namespace ServerApp.Config {
    public class ServerSettings{
        public string Host { get; set; }
        public string Port { get; set; }
        public string Zone { get; set; }
        public string Room { get; set; }
        public string Version { get; set; }
        public string SaveFile { get; set; }
        public bool DaemonMode { get; set; }
        public long msPerFrame { get; set; }
        public int ThreadLoopSleep { get; set; }
    }
}
