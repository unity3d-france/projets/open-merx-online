﻿using MerxServer;
using MerxServer.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using ServerApp.Config;
using Sfs2X;
using System;
using System.IO;

//
// L'application responsable de la gestion du serveur, pour le moment elle est unique et ne lancer aucun autre processus. 
//
namespace ServerApp {
    class Program {
        private static string environmentName;

        static void Main(string[] args) {

            //Event pour récupérer les erreurs globales
            System.AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

            environmentName = Environment.GetEnvironmentVariable("ENVIRONMENT");

            // create service collection
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            // create service provider
            var serviceProvider = serviceCollection.BuildServiceProvider();

            // entry to run app
            serviceProvider.GetService<App>().Run();
        }


        private static void ConfigureServices(IServiceCollection services) {
            services.AddLogging(configure => configure.AddSerilog());

            var builder = new ConfigurationBuilder()
               .SetBasePath(Directory.GetCurrentDirectory())
               .AddJsonFile($"appsettings.json", optional: true, reloadOnChange: true)
               .AddJsonFile($"appsettings.{environmentName}.json", optional: true, reloadOnChange: true)
               .AddEnvironmentVariables();

            var configuration = builder.Build();

            //Pour pouvoir injecter les settings du serveur
            services.AddOptions();
            services.Configure<ServerSettings>(configuration.GetSection("ServerSettings"));

            //logger Configuration
            Log.Logger = new LoggerConfiguration()
              .ReadFrom.Configuration(configuration)
              .CreateLogger();

            services.AddSingleton<IMerxServerApp, MerxServerApp>()
                .AddTransient<IEventSender, SFSEventSender>()
                .AddSingleton<IConfiguration>(configuration)
                .AddSingleton<SFSInstance>()
                .AddScoped<SFSConnection>()
                .AddSingleton<App>();

        }

        static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e) {
            Console.WriteLine(e.ExceptionObject.ToString());
        }
    }
}
