using System;

namespace MerxData {

    /// <summary>
    /// une porte permetant de passer d'une City � une autre instantanement
    /// </summary>
    [Serializable]
    public class StarGate {
        /// <summary> la City de l'autre cote </summary>
        public City TargetCity{ get; set; }

        /// <summary> la distance entre cette porte et la destination, utilis�e pour le calcul du cout </summary>
        public double Distance { get; set; }
    }
}