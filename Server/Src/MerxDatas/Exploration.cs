using System;
using System.Collections.Generic;

namespace MerxData {

    /// <summary>
    /// represente une expedition de type exploratin
    /// Les exploration recherche des points d'interets autour d'une ville
    /// </summary>
    [Serializable]
    public class Exploration : Expedition{

        /// <summary> creation d'une expedition exploration</summary>
        public Exploration(Universe universe) : base(universe) {
        }

        protected override void GenerateReward() {
            //envoyer le mail
            Corporation corp = Universe.GetCorporation(_startHangar.CorpID);
            if (null == corp)
                return;

            MailBox mailBox = corp.GetMailBox();
            if (null == mailBox)
                return;

            Console.WriteLine("hangar = " + _startHangar.ToString());
            Console.WriteLine("mailbox = " + mailBox);


            int de100 =  Universe.Random.Range(100);
            Console.WriteLine("ici");
            if (de100 >= 90) {
                mailBox.AddMail(new MerxStructures.MailInfos("Expedition result", "Votre expedition s'est termin� et a trouver 10 roche10"));
                _startHangar.AddResource(13, 100);
            } else if (de100 > 80) {
                mailBox.AddMail(new MerxStructures.MailInfos("Expedition result", "Votre expedition s'est termin� et a trouver 100 roche9"));
                _startHangar.AddResource(12, 100);
            } else if (de100 > 70) {
                mailBox.AddMail(new MerxStructures.MailInfos("Expedition result", "Votre expedition s'est termin� et a trouver 100 roche8"));
                _startHangar.AddResource(11, 100);
            } else if (de100 > 60) {
                mailBox.AddMail(new MerxStructures.MailInfos("Expedition result", "Votre expedition s'est termin� et a trouver 100 roche7"));
                _startHangar.AddResource(10, 100);
            } else if (de100 > 50) {
                mailBox.AddMail(new MerxStructures.MailInfos("Expedition result", "Votre expedition s'est termin� et a trouver 100 roche6"));
                _startHangar.AddResource(9, 100);
            } else if (de100 > 40) {
                mailBox.AddMail(new MerxStructures.MailInfos("Expedition result", "Votre expedition s'est termin� et a trouver 100 roche5"));
                _startHangar.AddResource(8, 100);
            } else if (de100 > 30) {
                mailBox.AddMail(new MerxStructures.MailInfos("Expedition result", "Votre expedition s'est termin� et a trouver 100 roche4"));
                _startHangar.AddResource(7, 100);
            } else if (de100 > 20) {
                mailBox.AddMail(new MerxStructures.MailInfos("Expedition result", "Votre expedition s'est termin� et a trouver 100 roche3"));
                _startHangar.AddResource(6, 100);
            } else if (de100 > 10) {
                mailBox.AddMail(new MerxStructures.MailInfos("Expedition result", "Votre expedition s'est termin� et a trouver 100 roche2"));
                _startHangar.AddResource(5, 100);
            } else if(de100 > 1) { 
                mailBox.AddMail(new MerxStructures.MailInfos("Expedition result", "Votre expedition s'est termin� et a trouver 100 roche1"));
                _startHangar.AddResource(4, 100);
            } else {
                mailBox.AddMail(new MerxStructures.MailInfos("Expedition result", "Votre expedition est termin�e et n'a rien trouv�e"));
            }
            Console.WriteLine("ici la");
        }
    }
}