using System;
using System.Collections.Generic;
using System.Linq;

using MerxStructures;

namespace MerxData {

    /// <summary>
    /// Represente une ville dans le modele de donn�e. Une ville est a une position dans la carte
    /// Elle contient des hangars et des usines
    /// </summary>
    [Serializable]
    public class City : GameEntity{

        /// <summary> les hangars dans la ville, la cl� etant l'id de la corporation </summary>
        private Dictionary<int, Hangar> _hangars = null; //

        /// <summary> la liste des recettes que peut ex�cuter cette ville </summary>
        private List<Recipe> _recipies = new List<Recipe>();

        /// <summary> Le nombre de frame entre chaque update du market </summary>
        private int _frameBetweenMarketUpdate = 3000; // 30 sec

        /// <summary> la recette que la station essais de faire presentement </summary>
        private Recipe _CurrentRecipe = null;
        /// <summary> l'importance de finir cette recette, c'est utilise pour fixer les prix, une importance normale = 1.0 </summary>
        private float _RecipeImportance = 1.0f;

        /// <summary> le dernier frame auquel cette City a ajust� ses posistions sur le market </summary>
        private long _lastMarketUpdate = -100000; // cette valeur c'est pour faire un update au frame 1
        
        /// <summary> les portes autour de cette City, la cl� �tant l'id de la City de l'autre cote de la porte
        /// </summary>
        private Dictionary<int, StarGate> _gates = null;
        /// <summary> recupere la liste des gates de cette City, il s'agit d'une copie de l'information </summary>
        public Dictionary<int,StarGate> Gates { get { return new Dictionary<int, StarGate>(_gates); } }
   
        /// <summary> Les factories de la City</summary>
        public List<Factory> Factories { get; private set; }
                
        public string Name { get; set; }

        /// <summary> Creation d'une City </summary>
        /// <param name="universe">L'univers dans laquel est cette City</param>
        public City(Universe universe) : base(universe) {
            _hangars = new Dictionary<int, Hangar>();
            Hangar h = new Hangar(this,0);
            Universe.AddEntity(h);
            _hangars.Add(0, h); // le hangar de la City elle meme

            _gates = new Dictionary<int, StarGate>();

            Factories = new List<Factory>();
            Factory factory = new Factory(this);
            Universe.AddEntity(factory);
            Factories.Add(factory);
            
            Universe.RegisterEvent(Universe.Frame + 10, this);
        }

        public override void Update(long currentFrame) {

            ///le hangar corpo 0 est celui du proprietaire de la City
            Hangar h = GetHangar(0);

            //toutes les City vendent le seul vehicule pour le moment
            if (h.GetResourceCount(3) + h.GetLockREsources(3) == 0)
                h.AddResource(3, 1);

            if (currentFrame >= _lastMarketUpdate + _frameBetweenMarketUpdate) {
                //tester si on a besoin de resources 
                ManageMarket();                
                _lastMarketUpdate = currentFrame;
            }

            StartProduction();

            SellStuff();

            //on va faire un update a toutes les 10 Frames
            Universe.RegisterEvent(Universe.Frame + 10, this);
        }

        /// <summary>
        /// ajouter un hangar dans la ville
        /// </summary>
        /// <param name="corpID">la corp qui sera proprietaire de ce hangar</param>
        /// <returns></returns>
        public Hangar AddHangar(int corpID) {
            if (_hangars.ContainsKey(corpID))
                return _hangars[corpID];

            int id = Universe.NextUID;
            Hangar h = new Hangar(this, corpID);
            _hangars.Add(corpID, h);

            return h;
        }

        /// <summary>
        /// permet d'ajouter une recette a cette City
        /// </summary>
        /// <param name="r"></param>
        public void AddRecipe(Recipe r) {
            _recipies.Add(r);
        }

        /// <summary>
        /// ajouter une nouvelle porte autour de cette City permetant d'atteindre une autre City
        /// </summary>
        /// <param name="toCity">la City cible</param>
        /// <param name="distance">la distance avec cette City</param>
        /// <returns></returns>
        public StarGate AddGate(City toCity, double distance) {
            if (null == toCity)
                return null;

            if (!_gates.ContainsKey(toCity.ID)) {
                StarGate gate = new StarGate();
                gate.TargetCity = toCity;
                gate.Distance = distance;
                _gates.Add(toCity.ID, gate);
            }
            return _gates[toCity.ID];
        }

        /// <summary>
        /// recup�rer le hangar d'une corporation
        /// </summary>
        /// <param name="corpID">l'id de la corporation ou 0 pour r�cup�rer le hangar de la City NPC/param>
        /// <returns>le hangar ou null si pas de hangar pour la corporation</returns>
        public Hangar GetHangar(int corpID) {
            if (_hangars.ContainsKey(corpID)) {
                return _hangars[corpID];
            }
            return null;
        }

        /// <summary>
        /// Methode demande a la station de g�rer les besoin de ressources et de mettre a jour le marche en consequence
        /// </summary>
        private void ManageMarket() {

            Hangar h = GetHangar(0); // le hangar 0 est celui de la station

            if (null == _CurrentRecipe && _recipies.Count > 0) {
                //on choisi une recette au hasard
                _CurrentRecipe = _recipies[Universe.Random.Range(_recipies.Count)];
                _RecipeImportance = 0.75f;
            } else {
                _RecipeImportance += (1.50f - _RecipeImportance) * 0.1f;
            }

            if (null == _CurrentRecipe)
                return;

            //on etablis nos besoin pour la dite recette
            int missing = 0;
            foreach(var input in _CurrentRecipe.inputs) {                
                int inStock = h.GetResourceCount(input.Key);
                int needToBuy = Math.Max(0, input.Value - inStock);
                IEnumerable<MarketOrder> myOrders = Universe.Market.GetBuyOrder(input.Key).Where((o) => o.cityID == ID && o.corpID == 0);
                if (myOrders.Count() > 1) {
                    //si on aplusieurs offre on va r�duire ca un peu...
                    bool first = true;
                    foreach(var order in myOrders) {
                        if (!first) {
                            first = false;
                            needToBuy -= order.qte;
                        } else {
                            Universe.Market.RemoveOrder(order);
                        }
                    }                    
                }

                int basePrice = Universe.Resources[input.Key].BasePrice;
                int newPrice = Convert.ToInt32(basePrice * _RecipeImportance);

                missing += needToBuy;

                if (needToBuy > 0) {                    
                    if(myOrders.Count() > 0) {
                        MarketOrder o = myOrders.First();
                        o.qte = needToBuy;
                        o.price = newPrice;
                        Universe.Market.UpdateOrder(o);
                    } else {
                        Universe.Market.AddBuyOrder(h, input.Key, needToBuy, newPrice);
                    }
                }
            }

        }
            
        /// <summary>
        /// Lancer les recettes possible de lancer
        /// </summary>
        private void StartProduction() {
            ///le hangar corpo 0 est celui du proprietaire de la City
            Hangar h = GetHangar(0);

            //lancer la current recipe si possible
            if(null != _CurrentRecipe) {
                if (h.TestQtes(_CurrentRecipe.inputs)) {
                    foreach(Factory f in Factories) {
                        if (!f.Running) {
                            f.Start(h, _CurrentRecipe, 1);
                            _CurrentRecipe = null;
                            break;
                        }
                    }
                }
            }
            //lancer les productions si possible
            foreach (Recipe r in _recipies) {

                if (h.TestQtes(r.inputs)) {

                    foreach (Factory f in Factories) {
                        if (!f.Running) {
                            f.Start(h, r, 1);
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// la station vend les surplus qu'elle a qui ne sont pas necessaire dans sa recette actuelle
        /// </summary>
        private void SellStuff() {
            Dictionary<int, int> toKeep = null;  // la quantite de ressources a garder
            if (null == _CurrentRecipe) {
                toKeep = new Dictionary<int, int>();
            } else {
                toKeep = new Dictionary<int, int>(_CurrentRecipe.inputs);
            }

            Hangar h = GetHangar(0);

            List<int> resInStock = new List<int>(h.GetResources().Keys);
            foreach(int resID in resInStock) {
                int toSell = 0;
                int inStock = h.GetResourceCount(resID);
                if (toKeep.ContainsKey(resID)) {
                    if (toKeep[resID] < inStock) {
                        //on en a plus que ce qu'il faut, on garde juste ce qu'il faut
                        toSell = inStock - toKeep[resID];
                    }
                } else {
                    toSell = inStock;
                }
                if(toSell > 0) {
                    int price = Universe.Resources[resID].BasePrice;
                    Universe.Market.AddSellOrder(h, resID, toSell, price);
                }
            }
        }

        #region comparaisons

        public Dictionary<int,Hangar> GetHangars() {
            return _hangars;
        }

        public Factory GetFactory(int id) {
            return Factories[id];
        }

        public override string ToString() {
            return Name;
        }
        public override int GetHashCode() {
            return ID;
        }

        public override bool Equals(object obj) {

            if (!base.Equals(obj))
                return false;

            if (null == obj)
                return false;

            City other = obj as City;
            if (null == other)
                return false;


            return true;
        }

        #endregion
    }
}