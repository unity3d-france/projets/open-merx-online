using System;
using System.Collections.Generic;

namespace MerxMessage {

    /// <summary>
    /// Requete permetant de recuperer la liste des hangars d'une corporation
    /// la corporation est celle du joueur envoyant la requete
    /// </summary>
    [Serializable]
    public class RequestHangarList : Message {

        /// <summary> la liste des hangars pouvant etre vu par le joueur demandeur </summary>
        public List<MerxStructures.HangarInfos> hangars = new List<MerxStructures.HangarInfos>();
    }
}
        