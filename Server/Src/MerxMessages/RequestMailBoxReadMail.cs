using System;
using System.Collections.Generic;

namespace MerxMessage {

    /// <summary>
    /// demande le passage en lu d'un mail
    /// </summary>
    [Serializable]
    public class RequestMailBoxReadMail : Message {

        //l'id du mail dans la mailbox du joueur
        public int Id;
    }
}
        