﻿using NUnit.Framework;

namespace MerxData.tst {

    [TestFixture]
    public class HangarTest {        

        private Universe _universe = null;
        private City _city0 = null;
        private Hangar _hangar = null;

        [SetUp]
        public void Init() {
            _universe = new Universe(0);

            _city0 = _universe.GetCity("Albanel");

            _hangar = _city0.AddHangar(1);
        }

        [Test]
        public void AddStuff() {

            _hangar.AddResource(1, 100);
            Assert.AreEqual(100, _hangar.GetResourceCount(1));
                        
        }

        [Test]
        public void UniverseSerializationWithChanges() {

            _hangar.AddResource(1, 100);

            System.Runtime.Serialization.Formatters.Binary.BinaryFormatter f = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            f.Serialize(stream, _universe);

            stream.Position = 0;
            Universe result = f.Deserialize(stream) as Universe;           

            Assert.AreEqual(result, _universe);

            City s2 = result.GetCity("Albanel");
            Hangar h2 = s2.GetHangar(1);

            Assert.AreEqual(100, h2.GetResourceCount(1));
        }
    }
}
