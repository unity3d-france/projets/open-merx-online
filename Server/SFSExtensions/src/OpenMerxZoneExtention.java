import com.smartfoxserver.v2.core.SFSEventType;
import com.smartfoxserver.v2.extensions.SFSExtension;

public class OpenMerxZoneExtention extends SFSExtension {

	@Override
	public void init() {
		trace("**************************************");
		trace("        Init  zone extention          ");
		trace("**************************************");
		
		
		addEventHandler(SFSEventType.USER_LOGIN, LoginEventHandler.class);
		
	}

}
